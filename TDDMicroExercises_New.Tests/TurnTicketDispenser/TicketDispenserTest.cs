﻿using Moq;
using NUnit.Framework;
using TDDMicroExercises_Core.TurnTicketDispenser;
using TDDMicroExercises_Core.TurnTicketDispenser.Intefraces;

namespace TDDMicroExercises_Core.Tests.TurnTicketDispenser
{
    [TestFixture]
    public class TicketDispenserTest
    {
        Mock<ITurnTicketFactory> _turnTicketFactory;
        Mock<ITurnNumberSequenceWrapper> _turnNumberSequenceWrapper;
        [SetUp]
        public void SetUp()
        {
            _turnTicketFactory = new Mock<ITurnTicketFactory>();
            _turnNumberSequenceWrapper = new Mock<ITurnNumberSequenceWrapper>();
        }
        [Test]
        public void TicketDispenser_GetFirstNumber_ReturnsTicketOne()
        {
            _turnNumberSequenceWrapper.Setup(m => m.GetNextTurnNumberWrapper()).Returns(1);
            ITurnTicket ticket = new TurnTicket(1);
            _turnTicketFactory.Setup(m => m.CreateTicket(1)).Returns(ticket);

            ITicketDispenser sut = new TicketDispenser(_turnNumberSequenceWrapper.Object, _turnTicketFactory.Object);
            var result = sut.GetTurnTicket();
           
            _turnTicketFactory.Verify();
            _turnNumberSequenceWrapper.Verify();
            Assert.That(result.TurnNumber, Is.EqualTo(1));
        }
        [Test]
        [TestCase(2)]
        [TestCase(3)]
        [TestCase(4)]
        [TestCase(5)]
        public void TicketDispenser_GetTicketsNumber_ReturnsTicketWithProgressiveNumber(int personNumber)
        {
            _turnNumberSequenceWrapper.Setup(m => m.GetNextTurnNumberWrapper()).Returns(personNumber);
            ITurnTicket ticket = new TurnTicket(personNumber);
            _turnTicketFactory.Setup(m => m.CreateTicket(personNumber)).Returns(ticket);

            ITicketDispenser sut = new TicketDispenser(_turnNumberSequenceWrapper.Object, _turnTicketFactory.Object);
            var result = sut.GetTurnTicket();

            _turnTicketFactory.Verify();
            _turnNumberSequenceWrapper.Verify();
            Assert.That(result.TurnNumber, Is.EqualTo(personNumber));
        }
    }
}
