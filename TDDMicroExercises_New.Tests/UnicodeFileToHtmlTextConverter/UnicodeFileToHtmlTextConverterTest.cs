﻿using Moq;
using NUnit.Framework;
using System.IO;
using System.IO.Abstractions.TestingHelpers;
using TDDMicroExercises_Core.UnicodeFileToHtmlTextConverter.Interfaces;

namespace TDDMicroExercises_Core.Tests.UnicodeFileToHtmlConverter
{
    [TestFixture]
    public class UnicodeFileToHtmlTextConverterTest
    {
        MockFileSystem _mockFileSystem;

        [SetUp]
        public void SetUp()
        {
            _mockFileSystem = new MockFileSystem();
            var mockInputFile = new MockFileData("Somethinginline1\nSomethinginline2\nSomethinginline3");
            _mockFileSystem.AddFile(@"C:\temp\in.txt", mockInputFile);
        }
        [Test]
        public void UnicodeFileToHtmlTextConverter_ValidFilePath_ReturnsConvertedHTMLMessage()
        {
            IUnicodeFileToHtmlTextConverter sut = new UnicodeFileToHtmlTextConverter.UnicodeFileToHtmlTextConverter(_mockFileSystem);
            var result = sut.ConvertToHtml(@"C:\temp\in.txt");
           
            Assert.That(result, Is.EqualTo("Somethinginline1<br />Somethinginline2<br />Somethinginline3<br />"));
        }
        [Test]
        public void UnicodeFileToHtmlTextConverter_FileDoesNotExistPath_ReturnsExceptionMessage()
        {
            IUnicodeFileToHtmlTextConverter sut = new UnicodeFileToHtmlTextConverter.UnicodeFileToHtmlTextConverter(_mockFileSystem);
           
            var ex = Assert.Throws<FileNotFoundException>(() => sut.ConvertToHtml(@"C:\temp\in3.txt"));
           
            Assert.That(ex.Message, Is.EqualTo("Wrong File path or File does not exist."));
        }
        [Test]
        public void UnicodeFileToHtmlTextConverter_NullFilePath_ReturnsExceptionMessage()
        {
            IUnicodeFileToHtmlTextConverter sut = new UnicodeFileToHtmlTextConverter.UnicodeFileToHtmlTextConverter(_mockFileSystem);

            var ex = Assert.Throws<FileNotFoundException>(() => sut.ConvertToHtml(null));

            Assert.That(ex.Message, Is.EqualTo("Wrong File path or File does not exist."));
        }
    }
}
