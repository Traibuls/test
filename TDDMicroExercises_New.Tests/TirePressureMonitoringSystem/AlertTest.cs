using Moq;
using NUnit.Framework;
using TDDMicroExercises_Core.TirePressureMonitoringSystem;
using TDDMicroExercises_Core.TirePressureMonitoringSystem.Interfaces;

namespace TDDMicroExercises_Core.Tests.TirePressureMonitoringSystem
{
    [TestFixture]
    public class AlertTest
    {
        Mock<ISensor> _sensor;

        [SetUp]
        public void SetUp()
        {
            _sensor = new Mock<ISensor>();
        }
        [Test]
        public void Check_Default_ReturnsFalse()
        {
            IAlarm sut = new Alarm(_sensor.Object);

            Assert.That(sut.AlarmOn, Is.EqualTo(false));
        }
        [Test]
        [TestCase(16.9)]
        [TestCase(16.8)]
        [TestCase(16)]
        public void Check_Less17_ReturnsTrue(double pressure)
        {
            _sensor.Setup(s => s.PopNextPressurePsiValue()).Returns(pressure);
            IAlarm sut = new Alarm(_sensor.Object);

            sut.Check();
            _sensor.Verify(y => y.PopNextPressurePsiValue(), Times.Once());
            Assert.That(sut.AlarmOn, Is.EqualTo(true));
        }
        [Test]
        [TestCase(21.1)]
        [TestCase(21.8)]
        [TestCase(24)]
        public void Check_GreaterThan21_ReturnsTrue(double pressure)
        {
            _sensor.Setup(s => s.PopNextPressurePsiValue()).Returns(pressure);
            IAlarm sut = new Alarm(_sensor.Object);

            sut.Check();
            _sensor.Verify(y => y.PopNextPressurePsiValue(), Times.Once());
            Assert.That(sut.AlarmOn, Is.EqualTo(true));
        }
        [Test]
        [TestCase(21)]
        [TestCase(17)]
        public void Check_Borders17And21_ReturnsFalse(double pressure)
        {
            _sensor.Setup(s => s.PopNextPressurePsiValue()).Returns(pressure);
            IAlarm sut = new Alarm(_sensor.Object);

            sut.Check();
            _sensor.Verify(y => y.PopNextPressurePsiValue(), Times.Once());
            Assert.That(sut.AlarmOn, Is.EqualTo(false));
        }
        [Test]
        [TestCase(17.01)]
        [TestCase(19.20)]
        [TestCase(20.99)]
        public void Check_GreaterThan17AndLessThand21_ReturnsFalse(double pressure)
        {
            _sensor.Setup(s => s.PopNextPressurePsiValue()).Returns(pressure);
            IAlarm sut = new Alarm(_sensor.Object);

            sut.Check();
            _sensor.Verify(y => y.PopNextPressurePsiValue(), Times.Once());
            Assert.That(sut.AlarmOn, Is.EqualTo(false));
        }
    }
}