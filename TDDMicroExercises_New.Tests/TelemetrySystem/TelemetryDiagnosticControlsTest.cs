using Moq;
using NUnit.Framework;
using TDDMicroExercises_Core.TelemetrySystem.Interfaces;
using TDDMicroExercises_Core.TelemetrySystem;
using System;

namespace TDDMicroExercises_Core.Tests.TelemetrySystem
{
    [TestFixture]
    public class TelemetryDiagnosticControlsTest
    {
        Mock<IRandomService> _randomService;
        string ResposeMessage =  "LAST TX rate................ 100 MBPS\r\n"
                    + "HIGHEST TX rate............. 100 MBPS\r\n"
                    + "LAST RX rate................ 100 MBPS\r\n"
                    + "HIGHEST RX rate............. 100 MBPS\r\n"
                    + "BIT RATE.................... 100000000\r\n"
                    + "WORD LEN.................... 16\r\n"
                    + "WORD/FRAME.................. 511\r\n"
                    + "BITS/FRAME.................. 8192\r\n"
                    + "MODULATION TYPE............. PCM/FM\r\n"
                    + "TX Digital Los.............. 0.75\r\n"
                    + "RX Digital Los.............. 0.10\r\n"
                    + "BEP Test.................... -5\r\n"
                    + "Local Rtrn Count............ 00\r\n"
                    + "Remote Rtrn Count........... 00";

        [SetUp]
        public void SetUp()
        {
           _randomService = new Mock<IRandomService>();
        }
        [Test]     
        public void CheckTransmission_OnlineStatus_ReturnsDiagnosticMessage()
        {
            _randomService.Setup(rs => rs.GenerateRandomNumber(1, 10)).Returns(1);
            ITelemetryClient tc = new TelemetryClient(_randomService.Object);
            ITelemetryDiagnosticControls sut = new TelemetryDiagnosticControls(tc);
            
            sut.CheckTransmission();
            _randomService.Verify(y => y.GenerateRandomNumber(1, 10), Times.Once());
            Assert.That(sut.DiagnosticInfo, Is.EqualTo(ResposeMessage));
        }
        [Test]
        public void CheckTransmission_OfflineStatus_ReturnsException()
        {
            _randomService.Setup(rs => rs.GenerateRandomNumber(1, 10)).Returns(3);
            ITelemetryClient tc = new TelemetryClient(_randomService.Object);
            ITelemetryDiagnosticControls sut = new TelemetryDiagnosticControls(tc);

            var ex = Assert.Throws<Exception>(() => sut.CheckTransmission());
            _randomService.Verify(y => y.GenerateRandomNumber(1, 10), Times.Exactly(3));

            Assert.That(ex.Message, Is.EqualTo("Unable to connect."));
        }
        [Test]
        public void CheckTransmission_Default_ReturnsEmtyDiagnosticInfo()
        {
            ITelemetryClient tc = new TelemetryClient(_randomService.Object);
            ITelemetryDiagnosticControls sut = new TelemetryDiagnosticControls(tc);

            Assert.That(sut.DiagnosticInfo, Is.EqualTo(string.Empty));
        }
    }
}