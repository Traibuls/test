﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TDDMicroExercises_Core.TirePressureMonitoringSystem.Interfaces
{
    public interface IAlarm {
        void Check();
        bool AlarmOn { get; }
    }
}
