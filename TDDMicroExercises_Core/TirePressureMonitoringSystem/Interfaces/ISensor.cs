﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TDDMicroExercises_Core.TirePressureMonitoringSystem.Interfaces
{
    public interface ISensor
    {
        double PopNextPressurePsiValue();
    }
}
