﻿using System;
using TDDMicroExercises_Core.UnicodeFileToHtmlTextConverter.Interfaces;

namespace TDDMicroExercises.UnicodeFileToHtmlTextConverter.SomeDependencies
{
    public class aTextConverterClient2
    {
        // A class with the only goal of simulating a dependency on UnicodeFileToHtmTextConverter
        // that has impact on the refactoring.


        private readonly IUnicodeFileToHtmlTextConverter _textConverter;

        public aTextConverterClient2(IUnicodeFileToHtmlTextConverter textConverter) { 
             _textConverter = textConverter;
        }
        public string ConvertToHtml(string FilePath)
        {
            return _textConverter.ConvertToHtml(FilePath);
        }
    }
}
