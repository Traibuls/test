﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TDDMicroExercises_Core.TelemetrySystem.Interfaces
{
    public interface ITelemetryClient
	{
		bool OnlineStatus { get; }
		void Connect(string telemetryServerConnectionString);
		void Disconnect();
		void Send(string message);
		string Receive();
	}
}
