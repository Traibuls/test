﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TDDMicroExercises_Core.TelemetrySystem.Interfaces
{
    public interface ITelemetryDiagnosticControls
    {
        string DiagnosticInfo { get; }
        void CheckTransmission();
    }
}
