﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TDDMicroExercises_Core.TurnTicketDispenser.Intefraces
{
    public interface ITicketDispenser
    {
        ITurnTicket GetTurnTicket();
    }
}
