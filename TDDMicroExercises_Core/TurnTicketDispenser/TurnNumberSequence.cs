﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TDDMicroExercises_Core.TurnTicketDispenser
{
    public class TurnNumberSequence
    {
        private static int _turnNumber = 3;

        public static int GetNextTurnNumber()
        {
            return _turnNumber;
        }
    }
}
