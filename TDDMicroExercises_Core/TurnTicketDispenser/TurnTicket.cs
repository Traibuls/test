﻿using System;
using System.Collections.Generic;
using System.Text;
using TDDMicroExercises_Core.TurnTicketDispenser.Intefraces;

namespace TDDMicroExercises_Core.TurnTicketDispenser
{
    public class TurnTicket : ITurnTicket
    {
        private readonly int _turnNumber;

        public TurnTicket(int turnNumber)
        {
            _turnNumber = turnNumber;
        }

        public int TurnNumber
        {
            get { return _turnNumber; }
        }

    }
}
