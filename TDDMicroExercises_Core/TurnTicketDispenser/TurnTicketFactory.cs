﻿using System;
using System.Collections.Generic;
using System.Text;
using TDDMicroExercises_Core.TurnTicketDispenser.Intefraces;

namespace TDDMicroExercises_Core.TurnTicketDispenser
{
    public class TurnTicketFactory : ITurnTicketFactory
    {
       public ITurnTicket CreateTicket(int number)
        {
            var result = new TurnTicket(number);
            return result;
        }
    }
}
