﻿using System;
using System.Collections.Generic;
using System.Text;
using TDDMicroExercises_Core.TurnTicketDispenser.Intefraces;

namespace TDDMicroExercises_Core.TurnTicketDispenser
{
    public class TurnNumberSequenceWrapper : ITurnNumberSequenceWrapper
    {
        public int GetNextTurnNumberWrapper()
        {
            return TurnNumberSequence.GetNextTurnNumber();
        }
    }
}
