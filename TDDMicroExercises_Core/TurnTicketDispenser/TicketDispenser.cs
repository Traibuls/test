﻿using System;
using System.Collections.Generic;
using System.Text;
using TDDMicroExercises_Core.TurnTicketDispenser.Intefraces;

namespace TDDMicroExercises_Core.TurnTicketDispenser
{
    public class TicketDispenser: ITicketDispenser
    {
        private readonly ITurnNumberSequenceWrapper _turnNumberSequenceWrapper;
        private readonly ITurnTicketFactory _turnTicketFactory;
        public TicketDispenser(ITurnNumberSequenceWrapper TurnNumberSequenceWrapper, ITurnTicketFactory TurnTicketFactory)
        {
            _turnNumberSequenceWrapper = TurnNumberSequenceWrapper;
            _turnTicketFactory = TurnTicketFactory;
        }
        public ITurnTicket GetTurnTicket()
        {
            int newTurnNumber = _turnNumberSequenceWrapper.GetNextTurnNumberWrapper();
            return _turnTicketFactory.CreateTicket(newTurnNumber);
        }
    }
}
