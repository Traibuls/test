﻿using System;
using TDDMicroExercises_Core.TelemetrySystem.Interfaces;

namespace TDDMicroExercises_Core.TelemetrySystem.SomeDependencies
{
    public class TelemetryDiagnosticControlsClient1
    {
        // A class with the only goal of simulating a dependency on TelemetryDiagnosticControls
        // that has impact on the refactoring.
        private readonly ITelemetryDiagnosticControls _teleDiagnostic;
		public TelemetryDiagnosticControlsClient1(ITelemetryDiagnosticControls teleDiagnostic)
		{
			_teleDiagnostic = teleDiagnostic;
		}
		public string Check()
		{
			_teleDiagnostic.CheckTransmission();

			return _teleDiagnostic.DiagnosticInfo;
		}
	}
}
