﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web;
using System.IO.Abstractions;
using TDDMicroExercises_Core.UnicodeFileToHtmlTextConverter.Interfaces;

namespace TDDMicroExercises_Core.UnicodeFileToHtmlTextConverter
{
    public class UnicodeFileToHtmlTextConverter : IUnicodeFileToHtmlTextConverter
    {
        private readonly IFileSystem _fileSystem;

        public UnicodeFileToHtmlTextConverter(IFileSystem fileSystem)
        {
            _fileSystem = fileSystem;
        }
        public UnicodeFileToHtmlTextConverter() : this(new FileSystem()) { }
        public string ConvertToHtml(string fullFilenameWithPath)
        {
            if (string.IsNullOrEmpty(fullFilenameWithPath) || !_fileSystem.File.Exists(fullFilenameWithPath))
                throw new FileNotFoundException("Wrong File path or File does not exist.");

            using (TextReader unicodeFileStream = _fileSystem.File.OpenText(fullFilenameWithPath))
            {
                string html = string.Empty;

                string line = unicodeFileStream.ReadLine();
                while (line != null)
                {
                    html += HttpUtility.HtmlEncode(line);
                    html += "<br />";
                    line = unicodeFileStream.ReadLine();
                }
                return html;
            }
        }
    }
}
