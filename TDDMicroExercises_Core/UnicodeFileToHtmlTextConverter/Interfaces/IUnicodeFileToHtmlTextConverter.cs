﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TDDMicroExercises_Core.UnicodeFileToHtmlTextConverter.Interfaces
{
    public interface IUnicodeFileToHtmlTextConverter
    {
        string ConvertToHtml(string fullFilenameWithPath);
    }
}
