﻿using TDDMicroExercises_Core.TurnTicketDispenser.Intefraces;

namespace TDDMicroExercises.TurnTicketDispenser.SomeDependencies
{
    public class TurnTicketClient
    {
		// A class with the only goal of simulating a dependency on TurnTicket
		// that has impact on the refactoring.
		public TurnTicketClient(ITurnTicketFactory factory)
        {
            var turnTicket1 = factory.CreateTicket(1);
			var turnTicket2 = factory.CreateTicket(2);
			var turnTicket3 = factory.CreateTicket(3);

			var num1 = turnTicket1.TurnNumber;
			var num2 = turnTicket2.TurnNumber;
			var num3 = turnTicket3.TurnNumber;
		}
    }
}
