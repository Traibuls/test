﻿using System;
using TDDMicroExercises_Core.TurnTicketDispenser.Intefraces;

namespace TDDMicroExercises.TurnTicketDispenser.SomeDependencies
{
    public class TurnTicketAndSequenceClient
    {
		// A class with the only goal of simulating a dependencies on 
		// TurnNumberSequence and TurnTicket that have impact on the refactoring.

		public TurnTicketAndSequenceClient(ITurnTicketFactory factory, ITurnNumberSequenceWrapper turnNumberSequenceWrapper)
        {
			
			var turnTicket1 = factory.CreateTicket(turnNumberSequenceWrapper.GetNextTurnNumberWrapper());
			var turnTicket2 = factory.CreateTicket(turnNumberSequenceWrapper.GetNextTurnNumberWrapper());
			var turnTicket3 = factory.CreateTicket(turnNumberSequenceWrapper.GetNextTurnNumberWrapper());
        }
    }
}
