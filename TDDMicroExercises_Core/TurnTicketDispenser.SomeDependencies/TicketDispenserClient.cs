﻿using System;
using TDDMicroExercises_Core.TurnTicketDispenser.Intefraces;

namespace TDDMicroExercises.TurnTicketDispenser.SomeDependencies
{
    public class TicketDispenserClient
    {
		// A class with the only goal of simulating a dependency on TicketDispenser
		// that has impact on the refactoring.
		
		public TicketDispenserClient(ITicketDispenser _ticketDispenser)
        {
			_ticketDispenser.GetTurnTicket();
			_ticketDispenser.GetTurnTicket();
			_ticketDispenser.GetTurnTicket();
		}
    }
}
