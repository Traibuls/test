﻿using System;
using TDDMicroExercises_Core.TurnTicketDispenser.Intefraces;

namespace TDDMicroExercises.TurnTicketDispenser.SomeDependencies
{
    public class TurnNumberSequenceClient
    {
		// A class with the only goal of simulating a dependency on TurnNumberSequence
		// that has impact on the refactoring.

		public TurnNumberSequenceClient(ITurnNumberSequenceWrapper turnNumberSequenceWrapper)
        {
            int nextUniqueTicketNumber;
            nextUniqueTicketNumber = turnNumberSequenceWrapper.GetNextTurnNumberWrapper();
			nextUniqueTicketNumber = turnNumberSequenceWrapper.GetNextTurnNumberWrapper();
			nextUniqueTicketNumber = turnNumberSequenceWrapper.GetNextTurnNumberWrapper();
		}
    }
}
