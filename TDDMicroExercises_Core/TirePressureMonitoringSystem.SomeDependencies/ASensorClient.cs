﻿using System;
using TDDMicroExercises_Core.TirePressureMonitoringSystem.Interfaces;

namespace TDDMicroExercises_Core.TirePressureMonitoringSystem.SomeDependencies
{
    public class ASensorClient      
    {
        // A class with the only goal of simulating a dependency on Sensor
        // that has impact on the refactoring.

        public ASensorClient(ISensor sensor)
        {
            double value = sensor.PopNextPressurePsiValue();
			value = sensor.PopNextPressurePsiValue();
			value = sensor.PopNextPressurePsiValue();
			value = sensor.PopNextPressurePsiValue();
		}
    }
}
