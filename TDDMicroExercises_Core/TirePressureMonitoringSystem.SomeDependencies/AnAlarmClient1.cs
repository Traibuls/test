﻿using System;
using TDDMicroExercises_Core.TirePressureMonitoringSystem;
using TDDMicroExercises_Core.TirePressureMonitoringSystem.Interfaces;

namespace TDDMicroExercises_Core.TirePressureMonitoringSystem.SomeDependencies
{
    public class AnAlarmClient1
    {
        // A class with the only goal of simulating a dependency on Alert
        // that has impact on the refactoring.
        private readonly IAlarm _anAlarm;
        public AnAlarmClient1(IAlarm alarm)
        {
            _anAlarm = alarm;
        }
        public bool CheckStatus {get {
            _anAlarm.Check();
            return  _anAlarm.AlarmOn;
          } 
        }
    }
}
