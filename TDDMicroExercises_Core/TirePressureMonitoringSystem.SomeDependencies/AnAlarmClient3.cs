﻿using System;
using TDDMicroExercises_Core.TirePressureMonitoringSystem.Interfaces;

namespace TDDMicroExercises_Core.TirePressureMonitoringSystem.SomeDependencies
{
    public class AnAlarmClient3
    {
        // A class with the only goal of simulating a dependency on Alert
        // that has impact on the refactoring.

        private IAlarm _anAlarm;

        public AnAlarmClient3(IAlarm anAlarm)
        {
            _anAlarm = anAlarm;
        }
        public void DoSomething() 
        {
			_anAlarm.Check();          
        }
		public void DoSomethingElse()
		{
			bool isAlarmOn = _anAlarm.AlarmOn;
		}
    }
}
