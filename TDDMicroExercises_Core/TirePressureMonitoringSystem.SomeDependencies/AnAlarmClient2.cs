﻿using System;
using TDDMicroExercises_Core.TirePressureMonitoringSystem.Interfaces;

namespace TDDMicroExercises_Core.TirePressureMonitoringSystem.SomeDependencies
{
    public class AnAlarmClient2
    {
        // A class with the only goal of simulating a dependency on Alert
        // that has impact on the refactoring.
        private readonly IAlarm _anAlarm;
        public AnAlarmClient2(IAlarm alarm)
        {
            _anAlarm = alarm;
        }
        private void DoSomething()
        {
            _anAlarm.Check();
            bool isAlarmOn = _anAlarm.AlarmOn;
        }
    }
}
